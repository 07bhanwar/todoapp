const Task = require('../models/task.model');

exports.tasks = function (req, res, next) {
    // res.send(Task.find().limit(10));
    Task.find(function(err, tasks){
        if (err) return next(err);
        res.send(tasks);
    }).limit(10);

};

exports.limited_task = function(req, res, next){
    Task.find((err, tasks)=>{
        if(err) return next(err);
        res.send(tasks);
    }).limit(parseInt(req.params.count));
}

exports.create_task = function (req, res, next) {
    let task = new Task({
        task_name: req.body.task,
        is_completed: 0,
        date_completion: "0000-00-00",
        date_added: Date()
    });
    task.save(function (err) {
        if (err) {
            res.send(err);
        }else{
            res.send({
                success:true,
                message:"Task Created Sucessfully",
                task: task
            });
        }
    })
};

exports.task_details = function (req, res, next) {
    Task.findById(req.params.id, function (err, task) {
        if (err) {
            res.send(err);
        }else{
            res.send(task);
        }
    });
};

exports.task_update = function (req, res, next) {
    Task.findById({_id:req.params.id}, (err, task)=>{
        if(err) res.send(err);
        Object.assign(task, req.body).save((err, task) => {
            if(err) res.send(err);
            res.send({
                success:true,
                message:"Task Updated Sucessfully",
                task: task
            });
        });
    });
};

exports.task_delete = function (req, res, next) {
    Task.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send(err);
        }else{
            res.send({
                success:true,
                message:"Task deleted Sucessfully",
            });
        }
    })
};