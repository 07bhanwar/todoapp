const express = require('express');
const router = express.Router();

const task_controller = require('../controllers/task.controller');

router.get('/', task_controller.tasks);
router.get('/count/:count', task_controller.limited_task);
router.post('/create', task_controller.create_task);
router.get('/:id', task_controller.task_details);
router.put('/:id/update', task_controller.task_update);
router.delete('/:id/delete', task_controller.task_delete);

module.exports = router;