const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const morgan = require('morgan');

const task = require('./routes/task.route');
const app = express();

const mongoose = require('mongoose');

if(config.util.getEnv('NODE_ENV') !=='test'){
    app.use(morgan('combined'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));

// get it from config
// let dev_db_url = 'mongodb://localhost:27017/user_task';
// let dev_db_url = 'mongodb://root:123456789@test.cluster-cfjwhq4y04cs.us-east-1.docdb.amazonaws.com:27017/?connectTimeoutMS=300000'
// let dev_db_url = 'docdb-2019-02-13-05-12-22.cfcgkhabj5kl.us-east-2.docdb.amazonaws.com'
// const mongoDB = config.db_url;

mongoose.connect(config.db_url);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/task', task);

let port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

module.exports = app;