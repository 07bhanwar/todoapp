let mongoose = require('mongoose');
let Task = require('../models/task.model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Tasks', ()=> {
    beforeEach((done)=>{
        Task.deleteMany({}, (err)=>{
            done();
        });
    });

    describe('/GET tasks', ()=>{
        it('it should Get all the tasks', (done)=>{
            // chai.request("http://localhost:3000")
            chai.request(server)
                .get('/task')
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

    describe('/POST task', ()=>{
        it("it should not post without task name", (done)=>{
            let task = {
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            }

            chai.request(server)
                .post('/task/create')
                .send(task)
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('task_name');
                    res.body.errors.task_name.should.have.property('kind').eql('required');
                    done();
                });
        });
        it("it should post the task", (done)=>{
            let task = {
                task:"Create Testing Task"
            }
            chai.request(server)
                .post('/task/create')
                .send(task)
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql("Task Created Sucessfully");
                    res.body.task.should.have.property('is_completed');
                    res.body.task.should.have.property('date_completion');
                    res.body.task.should.have.property('date_added');
                    done();
                });
        });
    });

    describe("/GET/:id task", ()=>{
        it("it should get the task by the given id", (done)=>{
            let task = new Task({
                task_name:"Create Testing Task",
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            });
            
            task.save((err, task)=>{
                chai.request(server)
                .get('/task/'+task.id)
                .send(task)
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property('task_name');
                    res.body.should.have.property('is_completed');
                    res.body.should.have.property('date_completion');
                    res.body.should.have.property('date_added');
                    res.body.should.have.property('_id').eql(task.id);
                    done();
                });
            });
        });

        it("it should get the task by the given id", (done)=>{
            let task = new Task({
                task_name:"Create Testing Task",
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            });
            task.save((err, task)=>{
                chai.request(server)
                .get('/task/test')
                .send(task)
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property('message');
                    res.body.should.have.property('name').eql("CastError");
                    res.body.should.have.property('kind').eql("ObjectId");
                    done();
                });
            });
        });
    });

    describe("/PUT/:id task", ()=>{
        it("it should get the task by the given id", (done)=>{
            let task = new Task({
                task_name:"Create Testing Task",
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            });
            
            task.save((err, task)=>{
                chai.request(server)
                .put('/task/'+task.id+'/update')
                .send({task_name:"Create Testing Task Update"})
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property('success');
                    res.body.should.have.property('task');
                    res.body.task.should.have.property('task_name').eql("Create Testing Task Update");
                    done();
                });
            });
        });
    });

    describe("/DELETE/:id task", ()=>{
        it("it should get the task by the given id", (done)=>{
            let task = new Task({
                task_name:"Create Testing Task",
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            });
            
            task.save((err, task)=>{
                chai.request(server)
                .delete('/task/'+task.id+'/delete')
                .send({task_name:"Create Testing Task Update"})
                .end((err, res)=>{
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property('success');
                    res.body.should.have.property('message').eql('Task deleted Sucessfully');
                    done();
                });
            });
        });
    });

    // TEST DRIVEN DEVELOPMENT
    describe("/GET task count", ()=>{
        it("it should return the given number of task", (done)=>{
            Task({
                task_name:"Create Testing Task",
                is_completed:0,
                date_completion:"0000-00-00",
                date_added:Date()
            }).save(()=>{
                Task({
                    task_name:"Create Testing Task",
                    is_completed:0,
                    date_completion:"0000-00-00",
                    date_added:Date()
                }).save(()=>{
                    Task({
                        task_name:"Create Testing Task",
                        is_completed:0,
                        date_completion:"0000-00-00",
                        date_added:Date()
                    }).save((err, task)=>{
                        chai.request(server)
                            .get("/task/count/2")
                            .end((err, res)=>{
                                res.should.have.status(200);
                                res.should.have.be.a("object");
                                res.body.length.should.be.eql(2);
                                done();
                            });
                    });
                });
            });
        });
    })

    describe('Test database connection', ()=>{
        it("It should connect to mongodb", (done)=>{
            mongoose.connect("mongodb://localhost:27017/user_task", (error, connect)=>{
                connect.should.be.a("object");
                connect.should.have.property("base");
                connect.should.have.property("port").eql(27017);
                should.not.exist(error);
                done();
            });
        });

        // it("It should connect to documentDB", (done)=>{
        //     mongoose.connect("mongodb://root:123456789@docdb-2019-02-13-11-19-34.cluster-cfcgkhabj5kl.us-east-2.docdb.amazonaws.com:27017", (error, connect)=>{
        //         connect.should.be.a("object");
        //         connect.should.have.property("base");
        //         connect.should.have.property("port").eql(27017);
        //         should.not.exist(error);
        //         done();
        //     });
        // });

        // it("It should not connect to mongodb", (done)=>{

        //     mongoose.connect("mongodb://localhost:270/user_task", (error)=>{
        //         // error.should.be.a("object");
        //         // error.should.have.property("base");
        //         // error.should.have.property("port").eql(27017);
        //         throw new 
        //         process.exit(1);
        //         console.log("WORKFIN");
        //     });
        //     done();
        // });
    });
});