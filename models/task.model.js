const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let TaskSchema = new Schema({
    task_name: {type: String, max: 100, required: true},
    is_completed: {type: String, max: 100},
    date_completion: {type: String, max: 100},
    date_added: {type: String, max: 100},
});

module.exports = mongoose.model('tasks', TaskSchema);